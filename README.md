# LabView_programs

LabView programs for measurements

## Description

Collection of LabView vi-s to control different instruments to characterize SiPM devices


## Structure
Project containes two folders:
*   **libs**        -> library for instrumnets;
*   **vi_s**        -> vi-s to run different measuremtns;

## VI-s:
Project has vi-s:

To controle instruments:
* **Motors** -> folder to control Thorlab translation stages. Tested with *LTS300* and *MTS25*;
* **LeCroy Wave Series Sequence AcquisitionV2.vi** -> to control Lecroy Z610 waverunner oscilloscope;

To do measurements:
* **DoIVvsWavelength1Diod.vi** -> to run a wavelength scan and at each wavelength measure the SiPM IV;
* **DoMUSICData.vi**           -> measure the MUSIC ASIC responce at different PZ parameters;
* **DoVbiasScan.vi**           -> run **DoMUSICData.vi** at different bias voltages;
* **I-V_6487_v3.vi**           -> run IV measuremnts with Keithley 6487 picoampermeter;


## Authors and acknowledgment
Developed by A. Nagai at University of Geneve, DPNC

## License
For open source projects, say how it is licensed.


