﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">Driver for Agilent 81110 Signal Generator</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*%!!!*Q(C=\&gt;9R4O.1%-&lt;R$U2"[ROAC&amp;2&lt;TB835K(5WUV(H:9ST2:IKTH#GBPA+]Q6=I6=Q@PX]Y1!1K1";&lt;8#ZC8/^Z\H`7)\4UBNOZ1O.*\;^/&amp;'@RMS$7TP,`[K=TR]UHB-WOBW/"Z\8JT`0')WV/!X`1@A_0\]&amp;:`S@`&gt;`^``(`;&gt;`XP=H"XXR*JX6,XB-7N#=:P03E?2*HO2*HO2*(O2"(O2"(O2"\O2/\O2/\O2/&lt;O2'&lt;O2'&lt;O2W7%P)23ZS76N*EMG4C:+C39&amp;E-"1F,YEH]33?R-.(*:\%EXA34_*BC"*0YEE]C3@R=*I34_**0)EH]6#K3&lt;,NZ(A3$_56?!*0Y!E]A9=J&amp;8A#1$":5$AI!E.":X!1?!*0Y/&amp;1A3@Q"*\!%XDI6O!*0)%H]!1?4GF8*:JGWMHR5%;/R`%Y(M@D?#ANR_.Y()`D=4R-*]@D?"S%-[&amp;4()+=EZQ"TA@(YXBYE_.R0)\(]4A?ONI&gt;]H:F*MWUE_-R0)&lt;(]"A?QU-*'2\$9XA-D_'BL!S0Y4%]BM@Q-*5-D_%R0!&lt;%G*4J:21T4D1''9(BY;]^,&gt;&lt;O5D3*N&lt;V_GP/$KHI!61_7[I&amp;201CK'[S[=;I&lt;ILL1KAOIOD#K,[T[)CKA;G*61&gt;6!\8H&gt;U1:;4^P3.L1V&lt;56&lt;U,LJV%]?O.`PN&gt;PN.!S$_L\8&gt;LP6:L02?LX7;L839L&amp;1VX80K^56_\S&gt;(&gt;;F?Y[(B_N@`9_\G[&gt;O?&gt;P`7@Z]@,D_0?6@W?:V[6^9'X8_[N]9LN&amp;@[UB#1A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Configuration Query" Type="Folder">
				<Item Name="Action-Status_Configuration Query.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Action-Status_Configuration Query.mnu"/>
				<Item Name="Query Activ State Compl Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Activ State Compl Output.vi"/>
				<Item Name="Query Activ State Normal Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Activ State Normal Output.vi"/>
				<Item Name="Query Activ State Of Limits.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Activ State Of Limits.vi"/>
				<Item Name="Query Arming Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Arming Frequency.vi"/>
				<Item Name="Query Arming Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Arming Period.vi"/>
				<Item Name="Query Channel Addition Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Channel Addition Of Outp.vi"/>
				<Item Name="Query Config Of PLL Reference.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Config Of PLL Reference.vi"/>
				<Item Name="Query Config Of Trigger Out.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Config Of Trigger Out.vi"/>
				<Item Name="Query Coupling Mode Of Trailing.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Coupling Mode Of Trailing.vi"/>
				<Item Name="Query Data Format Of Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Data Format Of Pattern.vi"/>
				<Item Name="Query High Low Level Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query High Low Level Of Outputs.vi"/>
				<Item Name="Query Hold Mode Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Delay.vi"/>
				<Item Name="Query Hold Mode Of Double Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Double Delay.vi"/>
				<Item Name="Query Hold Mode Of Edges.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Edges.vi"/>
				<Item Name="Query Hold Mode Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Width.vi"/>
				<Item Name="Query Impedance Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Impedance Of Output.vi"/>
				<Item Name="Query Load Compensation Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Load Compensation Of Outp.vi"/>
				<Item Name="Query Memory Data Into File.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Memory Data Into File.vi"/>
				<Item Name="Query Offset Amplitude Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Offset Amplitude Of Outp.vi"/>
				<Item Name="Query Patt Mem Data Into Buffer.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Patt Mem Data Into Buffer.vi"/>
				<Item Name="Query Polarity Mode Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Polarity Mode Of Outputs.vi"/>
				<Item Name="Query Pulse Type.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Pulse Type.vi"/>
				<Item Name="Query Treshold Imp Of Clock Inp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Treshold Imp Of Clock Inp.vi"/>
				<Item Name="Query Treshold Imp Of Ext Input.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Treshold Imp Of Ext Input.vi"/>
				<Item Name="Query Update Mode Of Patt Data.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Update Mode Of Patt Data.vi"/>
				<Item Name="Query Value Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Delay.vi"/>
				<Item Name="Query Value Of Double Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Double Delay.vi"/>
				<Item Name="Query Value Of Duty Cycle.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Duty Cycle.vi"/>
				<Item Name="Query Value Of Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Frequency.vi"/>
				<Item Name="Query Value Of Leading Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Leading Edge.vi"/>
				<Item Name="Query Value Of Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Period.vi"/>
				<Item Name="Query Value Of Trailing Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Trailing Delay.vi"/>
				<Item Name="Query Value Of Trailing Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Trailing Edge.vi"/>
				<Item Name="Query Value Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Width.vi"/>
				<Item Name="Query Volt Amp Limits Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Volt Amp Limits Of Output.vi"/>
				<Item Name="Recall Stored Setting From File.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Recall Stored Setting From File.vi"/>
			</Item>
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Calibration Of The Tim System.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Calibration Of The Tim System.vi"/>
			<Item Name="Start Meas Of Freq At Clck In.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Start Meas Of Freq At Clck In.vi"/>
			<Item Name="Start Meas Of Period At Clck In.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Start Meas Of Period At Clck In.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Operation Mode" Type="Folder">
				<Item Name="Configure_Operation Mode.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Configure_Operation Mode.mnu"/>
				<Item Name="Gener Pulses By Ext Signal.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Gener Pulses By Ext Signal.vi"/>
				<Item Name="Generating Cont Burst.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Cont Burst.vi"/>
				<Item Name="Generating Cont Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Cont Pattern.vi"/>
				<Item Name="Generating Cont Pulses.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Cont Pulses.vi"/>
				<Item Name="Generating Gated Burst.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Gated Burst.vi"/>
				<Item Name="Generating Gated Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Gated Pattern.vi"/>
				<Item Name="Generating Gated Pulses.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Gated Pulses.vi"/>
				<Item Name="Generating Trigg Burst.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Trigg Burst.vi"/>
				<Item Name="Generating Trigg Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Trigg Pattern.vi"/>
				<Item Name="Generating Trigg Pulses.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Trigg Pulses.vi"/>
				<Item Name="Select Pulse Type Single Double.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Select Pulse Type Single Double.vi"/>
			</Item>
			<Item Name="Output" Type="Folder">
				<Item Name="Configure_Output.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Configure_Output.mnu"/>
				<Item Name="Activ Deactiv Complement Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Activ Deactiv Complement Output.vi"/>
				<Item Name="Activ Deactivate Normal Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Activ Deactivate Normal Output.vi"/>
				<Item Name="Configure Trigger Out.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Configure Trigger Out.vi"/>
				<Item Name="Select Activ State Of Limits.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Select Activ State Of Limits.vi"/>
				<Item Name="Select Channel Addition Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Select Channel Addition Of Outp.vi"/>
				<Item Name="Select Polarity Mode Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Select Polarity Mode Of Outputs.vi"/>
				<Item Name="Set High Low Level Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set High Low Level Of Outputs.vi"/>
				<Item Name="Set Impedance Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Impedance Of Output.vi"/>
				<Item Name="Set Load Compensation Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Load Compensation Of Output.vi"/>
				<Item Name="Set Offset Amplitude Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Offset Amplitude Of Outputs.vi"/>
				<Item Name="Set Volt Ampere Limits Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Volt Ampere Limits Of Outp.vi"/>
			</Item>
			<Item Name="Pattern Generation" Type="Folder">
				<Item Name="Configure_Pattern Generation.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Configure_Pattern Generation.mnu"/>
				<Item Name="Select Data Format Of Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Select Data Format Of Pattern.vi"/>
				<Item Name="Set Patt Data Via PRBS Polynom.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Set Patt Data Via PRBS Polynom.vi"/>
				<Item Name="Set Pattern Data Via Divider.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Set Pattern Data Via Divider.vi"/>
				<Item Name="Set Update Mode Of Pattern Data.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Set Update Mode Of Pattern Data.vi"/>
				<Item Name="Transfer Buff Data Into Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Transfer Buff Data Into Memory.vi"/>
				<Item Name="Transfer File Data Into Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Transfer File Data Into Memory.vi"/>
			</Item>
			<Item Name="Timing System" Type="Folder">
				<Item Name="Configure_Timing System.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Configure_Timing System.mnu"/>
				<Item Name="Select Coupl Mode Of Trailing.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Coupl Mode Of Trailing.vi"/>
				<Item Name="Select Hold Mode Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Delay.vi"/>
				<Item Name="Select Hold Mode Of Double Del.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Double Del.vi"/>
				<Item Name="Select Hold Mode Of Edges.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Edges.vi"/>
				<Item Name="Select Hold Mode Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Width.vi"/>
				<Item Name="Set Value Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Delay.vi"/>
				<Item Name="Set Value Of Double Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Double Delay.vi"/>
				<Item Name="Set Value Of Duty Cycle.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Duty Cycle.vi"/>
				<Item Name="Set Value Of Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Frequency.vi"/>
				<Item Name="Set Value Of Leading Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Leading Edge.vi"/>
				<Item Name="Set Value Of Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Period.vi"/>
				<Item Name="Set Value Of Trailing Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Trailing Delay.vi"/>
				<Item Name="Set Value Of Trailing Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Trailing Edge.vi"/>
				<Item Name="Set Value Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Width.vi"/>
			</Item>
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Configure.mnu"/>
			<Item Name="Configure PLL Reference.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Configure PLL Reference.vi"/>
			<Item Name="Level Unit.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Level Unit.vi"/>
			<Item Name="Recall Stored From Int Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Recall Stored From Int Memory.vi"/>
			<Item Name="Set Arming Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Arming Frequency.vi"/>
			<Item Name="Set Arming Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Arming Period.vi"/>
			<Item Name="Set Treshold Imp Of Clock Input.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Treshold Imp Of Clock Input.vi"/>
			<Item Name="Set Treshold Imp Of Extern Inp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Treshold Imp Of Extern Inp.vi"/>
			<Item Name="Store Act Setting In Int Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Store Act Setting In Int Memory.vi"/>
			<Item Name="Store Actual Setting In File.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Store Actual Setting In File.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Utility.mnu"/>
			<Item Name="Device Clear.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Device Clear.vi"/>
			<Item Name="Error Message.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Error Message.vi"/>
			<Item Name="Error Query (Multiple).vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Error Query (Multiple).vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Error Query.vi"/>
			<Item Name="Operation Complete.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Operation Complete.vi"/>
			<Item Name="Query Timeout.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Query Timeout.vi"/>
			<Item Name="Read Oper Condition Register.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Oper Condition Register.vi"/>
			<Item Name="Read Oper Event Status Reg.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Oper Event Status Reg.vi"/>
			<Item Name="Read Quest Condition Register.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Quest Condition Register.vi"/>
			<Item Name="Read Questionable Event Reg.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Questionable Event Reg.vi"/>
			<Item Name="Read Status Byte.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Status Byte.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Self-Test.vi"/>
			<Item Name="Set Timeout.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Set Timeout.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Query Attribute.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Query Attribute.vi"/>
		<Item Name="Set Attribute.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Set Attribute.vi"/>
		<Item Name="Utility Clean Up Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Utility Clean Up Initialize.vi"/>
		<Item Name="Utility Default Instrument Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Utility Default Instrument Setup.vi"/>
	</Item>
	<Item Name="Agilent 81110 Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Agilent 81110 Readme.html"/>
</Library>
