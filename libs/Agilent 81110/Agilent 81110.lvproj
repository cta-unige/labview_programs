<?xml version='1.0'?>
<Project Type="Project" LVVersion="8208000">
   <Property Name="Instrument Driver" Type="Str">True</Property>
   <Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
   <Item Name="My Computer" Type="My Computer">
      <Property Name="CCSymbols" Type="Str">OS_hidden,Win;CPU_hidden,x86;OS,Win;CPU,x86;</Property>
      <Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.tcp.enabled" Type="Bool">false</Property>
      <Property Name="server.tcp.port" Type="Int">3363</Property>
      <Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
      <Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="Examples" Type="Folder">
         <Item Name="Agilent 81110 Dual Channel Model Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Examples/Agilent 81110 Dual Channel Model Setup.vi"/>
         <Item Name="Agilent 81110.bin3" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Examples/Agilent 81110.bin3"/>
      </Item>
      <Item Name="Agilent 81110.lvlib" Type="Library" URL="/&lt;instrlib&gt;/Agilent 81110/Agilent 81110.lvlib">
         <Item Name="Public" Type="Folder">
            <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            <Item Name="Action-Status" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Configuration Query" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Action-Status_Configuration Query.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Action-Status_Configuration Query.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Activ State Compl Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Activ State Compl Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Activ State Normal Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Activ State Normal Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Activ State Of Limits.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Activ State Of Limits.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Arming Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Arming Frequency.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Arming Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Arming Period.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Channel Addition Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Channel Addition Of Outp.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Config Of PLL Reference.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Config Of PLL Reference.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Config Of Trigger Out.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Config Of Trigger Out.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Coupling Mode Of Trailing.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Coupling Mode Of Trailing.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Data Format Of Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Data Format Of Pattern.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query High Low Level Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query High Low Level Of Outputs.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Hold Mode Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Hold Mode Of Double Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Double Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Hold Mode Of Edges.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Edges.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Hold Mode Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Hold Mode Of Width.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Impedance Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Impedance Of Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Load Compensation Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Load Compensation Of Outp.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Memory Data Into File.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Memory Data Into File.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Offset Amplitude Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Offset Amplitude Of Outp.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Patt Mem Data Into Buffer.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Patt Mem Data Into Buffer.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Polarity Mode Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Polarity Mode Of Outputs.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Pulse Type.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Pulse Type.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Treshold Imp Of Clock Inp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Treshold Imp Of Clock Inp.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Treshold Imp Of Ext Input.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Treshold Imp Of Ext Input.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Update Mode Of Patt Data.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Update Mode Of Patt Data.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Double Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Double Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Duty Cycle.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Duty Cycle.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Frequency.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Leading Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Leading Edge.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Period.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Trailing Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Trailing Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Trailing Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Trailing Edge.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Value Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Value Of Width.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Query Volt Amp Limits Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Query Volt Amp Limits Of Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Recall Stored Setting From File.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Configuration Query/Recall Stored Setting From File.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Action-Status.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Calibration Of The Tim System.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Calibration Of The Tim System.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Start Meas Of Freq At Clck In.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Start Meas Of Freq At Clck In.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Start Meas Of Period At Clck In.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Action-Status/Start Meas Of Period At Clck In.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="Configure" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Operation Mode" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configure_Operation Mode.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Configure_Operation Mode.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Gener Pulses By Ext Signal.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Gener Pulses By Ext Signal.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Cont Burst.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Cont Burst.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Cont Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Cont Pattern.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Cont Pulses.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Cont Pulses.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Gated Burst.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Gated Burst.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Gated Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Gated Pattern.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Gated Pulses.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Gated Pulses.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Trigg Burst.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Trigg Burst.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Trigg Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Trigg Pattern.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Generating Trigg Pulses.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Generating Trigg Pulses.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Pulse Type Single Double.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Operation Mode/Select Pulse Type Single Double.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Output" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configure_Output.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Configure_Output.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Activ Deactiv Complement Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Activ Deactiv Complement Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Activ Deactivate Normal Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Activ Deactivate Normal Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Trigger Out.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Configure Trigger Out.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Activ State Of Limits.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Select Activ State Of Limits.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Channel Addition Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Select Channel Addition Of Outp.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Polarity Mode Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Select Polarity Mode Of Outputs.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set High Low Level Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set High Low Level Of Outputs.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Impedance Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Impedance Of Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Load Compensation Of Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Load Compensation Of Output.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Offset Amplitude Of Outputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Offset Amplitude Of Outputs.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Volt Ampere Limits Of Outp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Output/Set Volt Ampere Limits Of Outp.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Pattern Generation" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configure_Pattern Generation.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Configure_Pattern Generation.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Data Format Of Pattern.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Select Data Format Of Pattern.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Patt Data Via PRBS Polynom.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Set Patt Data Via PRBS Polynom.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Pattern Data Via Divider.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Set Pattern Data Via Divider.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Update Mode Of Pattern Data.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Set Update Mode Of Pattern Data.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Transfer Buff Data Into Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Transfer Buff Data Into Memory.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Transfer File Data Into Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Pattern Generation/Transfer File Data Into Memory.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Timing System" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configure_Timing System.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Configure_Timing System.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Coupl Mode Of Trailing.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Coupl Mode Of Trailing.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Hold Mode Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Hold Mode Of Double Del.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Double Del.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Hold Mode Of Edges.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Edges.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Select Hold Mode Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Select Hold Mode Of Width.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Double Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Double Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Duty Cycle.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Duty Cycle.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Frequency.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Leading Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Leading Edge.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Period.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Trailing Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Trailing Delay.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Trailing Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Trailing Edge.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Set Value Of Width.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Timing System/Set Value Of Width.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Configure.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Configure PLL Reference.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Configure PLL Reference.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Level Unit.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Level Unit.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Recall Stored From Int Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Recall Stored From Int Memory.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Set Arming Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Arming Frequency.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Set Arming Period.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Arming Period.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Set Treshold Imp Of Clock Input.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Treshold Imp Of Clock Input.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Set Treshold Imp Of Extern Inp.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Set Treshold Imp Of Extern Inp.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Store Act Setting In Int Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Store Act Setting In Int Memory.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Store Actual Setting In File.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Configure/Store Actual Setting In File.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="Utility" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Utility.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Device Clear.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Device Clear.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Error Message.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Error Message.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Error Query (Multiple).vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Error Query (Multiple).vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Error Query.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Operation Complete.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Operation Complete.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Query Timeout.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Query Timeout.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Oper Condition Register.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Oper Condition Register.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Oper Event Status Reg.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Oper Event Status Reg.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Quest Condition Register.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Quest Condition Register.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Questionable Event Reg.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Questionable Event Reg.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Status Byte.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Read Status Byte.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Reset.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Revision Query.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Self-Test.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Set Timeout.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Utility/Set Timeout.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Public/dir.mnu">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Close.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/Initialize.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Public/VI Tree.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
         </Item>
         <Item Name="Private" Type="Folder">
            <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            <Item Name="Query Attribute.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Query Attribute.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Set Attribute.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Set Attribute.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Utility Clean Up Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Utility Clean Up Initialize.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Utility Default Instrument Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 81110/Private/Utility Default Instrument Setup.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
         </Item>
         <Item Name="Agilent 81110 Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Agilent 81110/Agilent 81110 Readme.html">
            <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
         </Item>
      </Item>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
